//
//  TRTestFramework.h
//  TRTestFramework
//
//  Created by Roni Leyes on 2023-05-17.
//

#import <Foundation/Foundation.h>

//! Project version number for TRTestFramework.
FOUNDATION_EXPORT double TRTestFrameworkVersionNumber;

//! Project version string for TRTestFramework.
FOUNDATION_EXPORT const unsigned char TRTestFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TRTestFramework/PublicHeader.h>


