Pod::Spec.new do |s|
  s.name = 'TRTestFrameworkSPM'
  s.version = '1.0.0'
  s.author = 'RLTR'
  s.dependency = 'lottie-ios', '4.0.1'
  s.license = "MIT"
  s.homepage = 'http://www.google.com'
  s.source = { :http => 'https://gitlab.com/RoniLey/trtestframeworkspm/-/raw/main/TRTestFramework.zip' }
  s.summary = 'TRTestFrameworkSPM'
  s.documentation_url = 'http://www.google.com'
  s.ios.vendored_frameworks = 'TRTestFramework.xcframework'
  s.ios.deployment_target = '14.0'
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'x86_64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'x86_64' }
  s.source_files = 'TRTestFramework/**/*'
end

