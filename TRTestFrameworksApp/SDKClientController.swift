//
// Created by Trulioo (C) on 2022-11-22.
//

import UIKit
import TruliooSDK

class SDKClientController: UIViewController, TRTestProviderDelegate {
    func onInitialized() { TruliooSDK.Trulioo().launch(delegate: self) }
    func onComplete(result: TruliooSDK.TruliooSuccess) {}
    func onError(error: TruliooSDK.TruliooError) {}
    func onException(exception: TruliooSDK.TruliooException) {}
    
    func withShortCodeOrNull(shortCode: String?) {
        launchTrulioo()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        launchTrulioo()
    }
    
    @IBAction func onQRCodeButtonPressed(_ sender: Any) {
        TruliooSDK.Trulioo().getSessionResult()
    }

    @objc func launchTrulioo() {
        TruliooSDK.Trulioo().initialize()
        TruliooSDK.Trulioo().launch(delegate: self)
    }
}
